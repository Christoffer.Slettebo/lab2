package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    ArrayList <FridgeItem> itemsInFridge = new ArrayList<>();
    int maxSize = 20;

    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        return itemsInFridge.size();
    }

    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if (nItemsInFridge()<20){
            itemsInFridge.add(item);
            return true;
        }
        else{
            return false;
        }
        
    }

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if (!itemsInFridge.contains(item)){
            throw  new NoSuchElementException("No such item in fridge");
        }
        else{
            itemsInFridge.remove(item);
        } 
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        itemsInFridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        List<FridgeItem> expiredItems = new ArrayList<>();
        for(FridgeItem item : itemsInFridge){
            if (item.hasExpired()){
                expiredItems.add(item);
            }
        }
        for(FridgeItem item : expiredItems){
            takeOut(item);;
        }
        return expiredItems;
    }

}
